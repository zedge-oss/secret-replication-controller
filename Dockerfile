FROM debian:stretch
ENV OPERATOR_NAME=secret-replication-controller
COPY bin/manager /
ENTRYPOINT ["/manager"]
