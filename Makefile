
# Image URL to use all building/pushing image targets
IMG ?= controller:latest

all: test build

# Run tests
test:
	@go test ./pkg/... ./cmd/...	

coverage: ## Generate global code coverage report
	./tools/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	./tools/coverage.sh html;

build: fmt vet
	@go build -o bin/manager gitlab.com/zedge/secret-replication-controller/cmd/manager

# Build manager binary
manager: fmt vet
	@go build -o bin/manager gitlab.com/zedge/secret-replication-controller/cmd/manager

# Run against the configured Kubernetes cluster in ~/.kube/config
run: fmt vet
	@go run ./cmd/manager/main.go

# Install CRDs into a cluster
install:
	kubectl apply -f deploy/crds/

install-example:
	kubectl apply -f deploy/cr/

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deploy: 
	@echo TODO

# Run go fmt against code
fmt:
	go fmt ./pkg/... ./cmd/...

# Run go vet against code
vet:
	go vet ./pkg/... ./cmd/...

# Generate code
generate:
	operator-sdk generate k8s
	operator-sdk generate openapi
