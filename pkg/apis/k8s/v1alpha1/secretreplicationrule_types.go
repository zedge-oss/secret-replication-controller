package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// SecretReplicationRuleSpec defines the desired state of SecretReplicationRule
// +k8s:openapi-gen=true
type SecretReplicationRuleSpec struct {
	// Namespace to replicate secrets from
	WatchNamespace string `json:"watchNamespace"`

	// List of protected namespaces it should not replicate secrets into. Defaults populates kube-system if not defined.
	NamespaceBlacklist []string `json:"namespaceBlacklist,omitempty"`

	SecretBlacklist []SecretBlacklistEntry `json:"secretBlacklist,omitempty"`

	// Enable for having replication rule inserting ownerReference if it doesn't have one and start owning this secret.
	// Defaults to false
	AdoptSecrets bool `json:"adoptSecrets,omitempty"`
}

type SecretBlacklistEntry struct {
	Name string `json:"name,omitempty"`
	Type string `json:"type,omitempty"`
}

// SecretReplicationRuleStatus defines the observed state of SecretReplicationRule
// +k8s:openapi-gen=true
type SecretReplicationRuleStatus struct {
	// Status of which namespaces the secrets has been replicated to
	Namespaces map[string]int `json:"namespaces,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// SecretReplicationRule is the Schema for the secretreplicationrules API
// +k8s:openapi-gen=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:shortName=srr
// +genclient:nonNamespaced
type SecretReplicationRule struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   SecretReplicationRuleSpec   `json:"spec,omitempty"`
	Status SecretReplicationRuleStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// SecretReplicationRuleList contains a list of SecretReplicationRule
type SecretReplicationRuleList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []SecretReplicationRule `json:"items"`
}

func init() {
	SchemeBuilder.Register(&SecretReplicationRule{}, &SecretReplicationRuleList{})
}
