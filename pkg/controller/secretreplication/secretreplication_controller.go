package secretreplication

import (
	"context"
	zedgev1alpha1 "gitlab.com/zedge/secret-replication-controller/pkg/apis/k8s/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"reflect"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("controller_secretreplication")

var controllerKind = zedgev1alpha1.SchemeGroupVersion.WithKind("SecretReplicationRule")

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new SecretReplication Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileSecretReplication{client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("secretreplication-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	err = c.Watch(&source.Kind{Type: &zedgev1alpha1.SecretReplicationRule{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	err = c.Watch(&source.Kind{Type: &corev1.Secret{}}, &handler.EnqueueRequestForOwner{
		IsController: false,
		OwnerType:    &zedgev1alpha1.SecretReplicationRule{},
	})

	err = c.Watch(&source.Kind{Type: &corev1.Secret{}}, &handler.EnqueueRequestsFromMapFunc{ToRequests: handler.ToRequestsFunc(func(o handler.MapObject) []reconcile.Request {
		secret := o.Object.(*corev1.Secret)
		reqs := make([]reconcile.Request, 0)
		if secret.Type == corev1.SecretTypeServiceAccountToken {
			return reqs
		}

		watchLogger := log.WithValues("Secret.Namespace", secret.Namespace, "Secret.Name", secret.Name, "watcher", "secretsAllNamespaces")

		rulesList := &zedgev1alpha1.SecretReplicationRuleList{}
		err = mgr.GetClient().List(context.TODO(), &client.ListOptions{}, rulesList)

		if err != nil {
			watchLogger.Error(err, "error listing secret replication rules")
			return reqs
		}

		watchLogger.Info("Rules found", "sizeOfRulesList", len(rulesList.Items))

	RULES:
		for _, rule := range rulesList.Items {
			if secret.Namespace != rule.Spec.WatchNamespace {
				watchLogger.Info("Skipping secret for not matching watchNamespace", "watchNamespace", rule.Spec.WatchNamespace, "rule", rule.Name)
				continue
			}
			for _, blacklist := range rule.Spec.SecretBlacklist {
				if string(secret.Type) == blacklist.Type || secret.Name == blacklist.Name {
					watchLogger.Info("Skipping secret for matching secretBlacklist", "secretType", secret.Type, "secretName", secret.Name, "rule", rule.Name)
					continue RULES
				}
			}

			reqs = append(reqs, reconcile.Request{NamespacedName: types.NamespacedName{Name: rule.Name}})
		}
		if len(reqs) > 0 {
			watchLogger.Info("Queue from Secrets All Namespaces watch", "size", len(reqs), "watcher", "secretsAllNamespaces")
		}
		return reqs
	})})

	err = c.Watch(&source.Kind{Type: &corev1.Namespace{}}, &handler.EnqueueRequestsFromMapFunc{ToRequests: handler.ToRequestsFunc(func(o handler.MapObject) []reconcile.Request {
		rulesList := &zedgev1alpha1.SecretReplicationRuleList{}
		err = mgr.GetClient().List(context.TODO(), &client.ListOptions{}, rulesList)

		namespace := o.Object.(*corev1.Namespace)
		watchLogger := log.WithValues("Namespace.Name", namespace.Name, "watcher", "namespaces")

		reqs := make([]reconcile.Request, 0)
		if err != nil {
			watchLogger.Error(err, "error listing secret replication rules")
			return reqs
		}

		for _, rule := range rulesList.Items {
			if !stringInSlice(namespace.Name, rule.Spec.NamespaceBlacklist) {
				reqs = append(reqs, reconcile.Request{NamespacedName: types.NamespacedName{Name: rule.Name}})
			} else {
				watchLogger.Info("Skipping blacklisted namespace", "skippedNamespace", namespace.Name, "rule", rule.Name)
			}
		}
		if len(reqs) > 0 {
			watchLogger.Info("Queuing from Namespaces watch", "size", len(reqs))
		}
		return reqs
	})})
	if err != nil {
		return err
	}

	return nil
}

// blank assignment to verify that ReconcileSecretReplication implements reconcile.Reconciler
var _ reconcile.Reconciler = &ReconcileSecretReplication{}

// ReconcileSecretReplication reconciles a SecretReplication object
type ReconcileSecretReplication struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	client client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a SecretReplication object and makes changes based on the state read
// and what is in the SecretReplication.Spec
// TODO(user): Modify this Reconcile function to implement your Controller logic.  This example creates
// a Pod as an example
// Note:
// The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileSecretReplication) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// Handles and fixes this bug: https://github.com/kubernetes-sigs/controller-runtime/commit/6cfbed364aece9fa862ff37c382386630df70bca
	request.Namespace = ""

	reqLogger := log.WithValues("Request.Namespace", request.Namespace, "Request.Name", request.Name)
	reqLogger.Info("Reconciling SecretReplication")

	// Fetch the SecretReplication srr
	srr := &zedgev1alpha1.SecretReplicationRule{}
	err := r.client.Get(context.TODO(), request.NamespacedName, srr)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	secrets, err := getSecretsAllNamespaces(r)
	if err != nil {
		reqLogger.Error(err, "error listing secret replication rules")
		return reconcile.Result{}, err
	}

	sourcesList := make([]corev1.Secret, 0)
	currentSecretsToAdopt := make(map[types.NamespacedName]corev1.Secret, 0)
	currentReplicatedSecrets := make(map[types.NamespacedName]corev1.Secret, 0)
	currentStatusIndex := make(map[string]int, 0)

	_, secretsFoundInWatchNamespace := secrets[srr.Spec.WatchNamespace]
	for _, secretsInNamespace := range secrets {
	SECRETS:
		for _, secret := range secretsInNamespace {
			//for _, secret := range secrets.Items {
			// Dont replicate service account secrets, this will simply turn into a mess.
			if secret.Type == corev1.SecretTypeServiceAccountToken {
				continue
			}

			//Adopt secret? need to be figured out before it tries to add it to source list which is used for defining create or update operations.
			if secretsFoundInWatchNamespace && srr.Spec.AdoptSecrets {
				srrOwnerFound := false
				if secret.GetOwnerReferences() != nil {
					for _, ownerRef := range secret.ObjectMeta.OwnerReferences {
						if ownerRef.Kind == srr.Kind {
							srrOwnerFound = true
							break
						}
					}
				}
				if _, found := secrets[srr.Spec.WatchNamespace][secret.Name]; found && secret.Namespace != srr.Spec.WatchNamespace && !srrOwnerFound {
					currentSecretsToAdopt[types.NamespacedName{Name: secret.Name, Namespace: secret.Namespace}] = secret
					reqLogger.Info("Adopting and taking ownership", "secretNamespace", secret.Namespace, "secretName", secret.Name, "watchedNamespace", srr.Spec.WatchNamespace)
					continue
				}
			}

			if srr.Spec.WatchNamespace == secret.Namespace {
				for _, blacklist := range srr.Spec.SecretBlacklist {
					if string(secret.Type) == blacklist.Type || secret.Name == blacklist.Name {
						continue SECRETS
					}
				}
				sourcesList = append(sourcesList, secret)
				continue

			}

			if secret.ObjectMeta.OwnerReferences == nil {
				continue
			}

			for _, ownerRef := range secret.ObjectMeta.OwnerReferences {
				if ownerRef.Kind == srr.Kind {
					if _, found := currentStatusIndex[secret.Namespace]; !found {
						currentStatusIndex[secret.Namespace] = 1
					} else {
						currentStatusIndex[secret.Namespace] = currentStatusIndex[secret.Namespace] + 1
					}
					currentReplicatedSecrets[types.NamespacedName{Name: secret.Name, Namespace: secret.Namespace}] = secret
				}
			}
		}
	}

	status := zedgev1alpha1.SecretReplicationRuleStatus{
		Namespaces: currentStatusIndex,
	}
	if !reflect.DeepEqual(srr.Status, status) {
		srr.Status = status
		if err = r.client.Status().Update(context.TODO(), srr); err != nil {
			return reconcile.Result{}, err
		}
	}

	allNamespaces := &corev1.NamespaceList{}
	err = r.client.List(context.Background(), &client.ListOptions{}, allNamespaces)
	if err != nil {
		return reconcile.Result{}, err
	}

	// TODO set proper length
	namespaces := make([]corev1.Namespace, 0)
	for _, namespace := range allNamespaces.Items {
		if srr.Spec.WatchNamespace == namespace.Name {
			continue
		}
		if srr.Spec.NamespaceBlacklist != nil && stringInSlice(namespace.Name, srr.Spec.NamespaceBlacklist) {
			continue
		}
		namespaces = append(namespaces, namespace)
	}

	newReplicatedSecrets := make(map[types.NamespacedName]corev1.Secret, 0)

	for _, sourceSecret := range sourcesList {
		for _, namespace := range namespaces {
			newSecret := sourceSecret.DeepCopy()
			newSecret.ObjectMeta = metav1.ObjectMeta{
				Namespace:   namespace.Name,
				Name:        sourceSecret.Name,
				Labels:      sourceSecret.Labels,
				Annotations: sourceSecret.Annotations,
			}
			newReplicatedSecrets[types.NamespacedName{Name: sourceSecret.Name, Namespace: namespace.Name}] = *newSecret
		}
	}

	todoCreate := make([]corev1.Secret, 0)
	todoUpdate := make([]corev1.Secret, 0)
	todoDelete := make([]corev1.Secret, 0)
	for newSecretName, newSecret := range newReplicatedSecrets {

		oldSecret, foundOldSecret := currentReplicatedSecrets[newSecretName]
		_, adoptSecret := currentSecretsToAdopt[newSecretName]

		// Do we need to add an owner reference to this srr?
		addSrrAsOwnerReference := true
		for _, oldOwnerReference := range oldSecret.GetOwnerReferences() {
			if oldOwnerReference.Kind == srr.Kind && oldOwnerReference.Name == srr.Name {
				addSrrAsOwnerReference = false
				break
			}
		}

		// Figure out which create or update operations we are doing.
		if !foundOldSecret && !adoptSecret {
			// Would be neat to have controller:true on first created resource, but it just leads to spamming an error message
			// Look into later...
			// if err := controllerutil.SetControllerReference(srr, &newSecret, r.scheme); err != nil {

			if err := AddControllerReference(srr, &newSecret, r.scheme); err != nil {
				return reconcile.Result{}, err
			}
			todoCreate = append(todoCreate, newSecret)
			reqLogger.Info("Scheduling create", "Namespace", newSecret.Namespace, "Name", newSecret.Name)
		} else if (foundOldSecret && addSrrAsOwnerReference) || !reflect.DeepEqual(oldSecret.Data, newSecret.Data) || !reflect.DeepEqual(oldSecret.Labels, newSecret.Labels) || !reflect.DeepEqual(oldSecret.Annotations, newSecret.Annotations) {
			newSecret.SetOwnerReferences(oldSecret.GetOwnerReferences())
			if err := AddControllerReference(srr, &newSecret, r.scheme); err != nil {
				return reconcile.Result{}, err
			}

			reqLogger.Info("Scheduling update", "Namespace", newSecret.Namespace, "Name", newSecret.Name)
			todoUpdate = append(todoUpdate, newSecret)
		}
	}
	for oldSecretName, oldSecret := range currentReplicatedSecrets {
		_, foundSecretToAdopt := currentSecretsToAdopt[types.NamespacedName{Name: oldSecret.Name, Namespace: oldSecret.Namespace}]

		// Dont delete secret if it is about to be adopted with ownerRefrence
		if _, found := newReplicatedSecrets[oldSecretName]; !found && !foundSecretToAdopt {
			reqLogger.Info("Scheduling delete", "Namespace", oldSecret.Namespace, "Name", oldSecret.Name)
			todoDelete = append(todoDelete, oldSecret)
		}
	}

	for _, newSecret := range todoCreate {
		if err = r.client.Create(context.Background(), &newSecret); err != nil {
			return reconcile.Result{}, err
		}
	}
	for _, updateSecret := range todoUpdate {
		if err = r.client.Update(context.Background(), &updateSecret); err != nil {
			return reconcile.Result{}, err
		}
	}
	for _, deleteSecret := range todoDelete {
		if err = r.client.Delete(context.Background(), &deleteSecret); err != nil {
			return reconcile.Result{}, err
		}
	}

	reqLogger.Info("Debug data: ", "SecretReplicationRule.Name", srr.Name)
	shouldRequeue := false
	if len(todoCreate) > 0 || len(todoUpdate) > 0 || len(todoDelete) > 0 {
		// Requeue to update status on next reconcile.
		shouldRequeue = true
	}
	return reconcile.Result{Requeue: shouldRequeue}, nil
}

type namespacedSecretMap map[string]map[string]corev1.Secret

// Map with namespaces which maps to secret names which again names to KEYs in secrets and their value.
// ie secretMap['default']['my-secret']
func getSecretsAllNamespaces(r *ReconcileSecretReplication) (namespacedSecretMap, error) {
	secrets := &corev1.SecretList{}
	if err := r.client.List(context.Background(), &client.ListOptions{Namespace: ""}, secrets); err != nil {
		return nil, err
	}
	secretMap := make(map[string]map[string]corev1.Secret)
	for _, s := range secrets.Items {
		if _, found := secretMap[s.Namespace]; !found {
			secretMap[s.Namespace] = make(map[string]corev1.Secret)
		}
		secretMap[s.Namespace][s.Name] = s
	}
	return secretMap, nil
}
