package secretreplication

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	zedgev1alpha1 "gitlab.com/zedge/secret-replication-controller/pkg/apis/k8s/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"strconv"
	"testing"
)

var (
	name      = "example-rule"
	namespace = ""

	myCredentialsSecretsName = "my-credentials"

	watchNamespace      = "it"
	defaultNamespace    = "default"
	kubeSystemNamespace = "kube-system"
	newNamespace        = "newNamespace"
)

func defaultBlacklist() []string {
	return []string{"kube-node-lease", "kube-public", "kube-system"}
}

func TestReplicateNewSRRFromWatchNamespace(t *testing.T) {
	// Set the logger to development mode for verbose logs.
	logf.SetLogger(logf.ZapLogger(true))

	ns := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: watchNamespace,
		},
	}
	nsDefault := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: defaultNamespace,
		},
	}
	nsKubeSystem := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: kubeSystemNamespace,
		},
	}

	// Secret 1
	s1 := createSecret(myCredentialsSecretsName, watchNamespace)

	srr := &zedgev1alpha1.SecretReplicationRule{
		TypeMeta: metav1.TypeMeta{
			Kind: "SecretReplicationRule",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: zedgev1alpha1.SecretReplicationRuleSpec{
			WatchNamespace:     watchNamespace,
			NamespaceBlacklist: defaultBlacklist(),
		},
	}

	// List of resources that will be created inside the mocked client.
	objs := []runtime.Object{
		srr,
		ns,
		nsDefault,
		nsKubeSystem,
		s1,
	}

	// Register operator types with the runtime scheme.
	s := scheme.Scheme
	s.AddKnownTypes(zedgev1alpha1.SchemeGroupVersion, srr)
	// Create a fake client to mock API calls.
	cl := fake.NewFakeClient(objs...)

	// Create a ReconcileSecretReplication object with the scheme and fake client.
	r := &ReconcileSecretReplication{client: cl, scheme: s}

	// Mock request to simulate Reconcile() being called on an event for a
	// watched resource .
	req := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      name,
			Namespace: namespace,
		},
	}
	res, err := r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if !res.Requeue {
		t.Error("expected requeue of reconcile request as it should have replicated a secret")
	}

	statusNamespaces := srr.Status.Namespaces
	if len(statusNamespaces) > 0 {
		t.Fatalf("Status gets updated on second reconcile")
	}

	secrets, err := getSecretsAllNamespaces(r)
	require.NoError(t, err)

	assert.Contains(t, secrets, "default")
	assert.Contains(t, secrets, "it")
	assert.NotContains(t, secrets, "kube-system")
	assert.Equal(t, "us3r1", string(secrets["default"][myCredentialsSecretsName].Data["username"]))
	assert.Equal(t, "us3r1", string(secrets["it"][myCredentialsSecretsName].Data["username"]))

	srr = reconcileExampleRule(r, t)
	assert.Len(t, srr.Status.Namespaces, 1)
}

func TestReplicatedSecretOwnerReferencesToBothSRRs(t *testing.T) {
	// Set the logger to development mode for verbose logs.
	logf.SetLogger(logf.ZapLogger(true))

	ns := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: watchNamespace,
		},
	}
	nsDefault := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: defaultNamespace,
		},
	}
	nsKubeSystem := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: kubeSystemNamespace,
		},
	}

	// Secret 1
	s1 := createSecret(myCredentialsSecretsName, watchNamespace)

	srr := &zedgev1alpha1.SecretReplicationRule{
		TypeMeta: metav1.TypeMeta{
			Kind: "SecretReplicationRule",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: zedgev1alpha1.SecretReplicationRuleSpec{
			WatchNamespace:     watchNamespace,
			NamespaceBlacklist: defaultBlacklist(),
		},
	}

	// List of resources that will be created inside the mocked client.
	objs := []runtime.Object{
		srr,
		ns,
		nsDefault,
		nsKubeSystem,
		s1,
	}

	// Register operator types with the runtime scheme.
	s := scheme.Scheme
	s.AddKnownTypes(zedgev1alpha1.SchemeGroupVersion, srr)
	// Create a fake client to mock API calls.
	cl := fake.NewFakeClient(objs...)

	// Create a ReconcileSecretReplication object with the scheme and fake client.
	r := &ReconcileSecretReplication{client: cl, scheme: s}

	// Mock request to simulate Reconcile() being called on an event for a
	// watched resource .
	req := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      name,
			Namespace: namespace,
		},
	}
	res, err := r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if !res.Requeue {
		t.Error("expected requeue of reconcile request as it should have replicated a secret")
	}

	srr2 := &zedgev1alpha1.SecretReplicationRule{
		TypeMeta: metav1.TypeMeta{
			Kind: "SecretReplicationRule",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name + "-2",
			Namespace: namespace,
		},
		Spec: zedgev1alpha1.SecretReplicationRuleSpec{
			WatchNamespace:     watchNamespace,
			NamespaceBlacklist: defaultBlacklist(),
		},
	}

	if err = r.client.Create(context.Background(), srr2); err != nil {
		t.Error("Failed to create SRR2 which is duplicate of SRR, just differnt names", err)
	}

	req = reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      "example-rule-2",
			Namespace: "",
		},
	}
	res, err = r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if !res.Requeue {
		t.Error("Expected it to update ownerReference")
	}

	// Below we are checking we got right amount of owners references.

	verifySecret := &corev1.Secret{}
	if err = r.client.Get(
		context.Background(),
		types.NamespacedName{Namespace: defaultNamespace, Name: myCredentialsSecretsName},
		verifySecret); err != nil {
		t.Error("Could not get secret to check ownerReferences", err)
	}

	assert.Len(t, verifySecret.OwnerReferences, 2)

	// Force another reconcile, still ensure we dont endless add ownerReferences
	res, err = r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if res.Requeue {
		t.Error("Expected no more reconciles")
	}

	if err = r.client.Get(
		context.Background(),
		types.NamespacedName{Namespace: defaultNamespace, Name: myCredentialsSecretsName},
		verifySecret); err != nil {
		t.Error("Could not get secret to check ownerReferences", err)
	}
	assert.Len(t, verifySecret.OwnerReferences, 2)

}

func reconcileExampleRule(r *ReconcileSecretReplication, t *testing.T) *zedgev1alpha1.SecretReplicationRule {
	req := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      "example-rule",
			Namespace: "",
		},
	}
	res, err := r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if res.Requeue {
		t.Error("Expected no more reconciles at this point")
	}
	// Check Status after Reconciles
	srr := &zedgev1alpha1.SecretReplicationRule{}
	err = r.client.Get(context.TODO(), req.NamespacedName, srr)
	if err != nil {
		t.Errorf("get srr: (%v)", err)
	}
	return srr
}

func TestNewNamespaceGetsReplicatedSecrets(t *testing.T) {
	// Set the logger to development mode for verbose logs.
	logf.SetLogger(logf.ZapLogger(true))

	ns := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: watchNamespace,
		},
	}
	nsDefault := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: defaultNamespace,
		},
	}
	nsKubeSystem := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: kubeSystemNamespace,
		},
	}

	// Secret 1
	s1 := createSecret(myCredentialsSecretsName, watchNamespace)

	srr := &zedgev1alpha1.SecretReplicationRule{
		TypeMeta: metav1.TypeMeta{
			Kind: "SecretReplicationRule",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
			UID:       "example-rule-uid-srr-1",
		},
		Spec: zedgev1alpha1.SecretReplicationRuleSpec{
			WatchNamespace:     watchNamespace,
			NamespaceBlacklist: defaultBlacklist(),
		},
	}

	s2 := createSecret(myCredentialsSecretsName, defaultNamespace)
	s2.OwnerReferences = []metav1.OwnerReference{*metav1.NewControllerRef(srr, controllerKind)}

	// List of resources that will be created inside the mocked client.
	objs := []runtime.Object{
		ns,
		nsDefault,
		nsKubeSystem,
		s2,
		s1,
		srr,
	}

	// Register operator types with the runtime scheme.
	s := scheme.Scheme
	s.AddKnownTypes(zedgev1alpha1.SchemeGroupVersion, srr)
	// Create a fake client to mock API calls.
	cl := fake.NewFakeClient(objs...)

	// Create a ReconcileSecretReplication object with the scheme and fake client.
	r := &ReconcileSecretReplication{client: cl, scheme: s}

	// Mock request to simulate Reconcile() being called on an event for a
	// watched resource .
	req := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      name,
			Namespace: namespace,
		},
	}
	res, err := r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if res.Requeue {
		t.Error("current state in cluster shouldn't have caused reconciles. see ojbs.")
	}

	nsNewNamespace := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: newNamespace,
		},
	}

	if err := r.client.Create(context.Background(), nsNewNamespace); err != nil {
		t.Fatal("Error creating new namespace")
	}

	req = reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      name,
			Namespace: namespace,
		},
	}
	res, err = r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if !res.Requeue {
		t.Error("current state in cluster should cause it to replicate secrets to new namespace")
	}

	secrets, err := getSecretsAllNamespaces(r)
	require.NoError(t, err)
	assert.Contains(t, secrets, newNamespace)

	srr = reconcileExampleRule(r, t)
	assert.Len(t, srr.Status.Namespaces, 2)
}

var resourcei = 0

func createSecret(secretName string, namespace string) *corev1.Secret {
	data := make(map[string][]byte)
	data["username"] = []byte("us3r1")
	data["password"] = []byte("p4ssw0rd2")
	resourcei = resourcei + 1
	secret := &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      secretName,
			Namespace: namespace,
			UID:       types.UID(secretName + "-uid-" + strconv.Itoa(resourcei)),
		},
		Data: data,
		Type: corev1.SecretTypeOpaque,
	}
	return secret
}

func TestDeleteSRRCleansUpReplicatedSecrets(t *testing.T) {
	// This needs to be an e2e it seems, its another controller kicking in to do the GC as I understand it.
}

func TestDeleteSecretInWatchNamespaceReplicatesDeletion(t *testing.T) {
	// Set the logger to development mode for verbose logs.
	logf.SetLogger(logf.ZapLogger(true))

	ns := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: watchNamespace,
		},
	}
	nsDefault := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: defaultNamespace,
		},
	}
	nsKubeSystem := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: kubeSystemNamespace,
		},
	}

	// Secret 1
	s1 := createSecret(myCredentialsSecretsName, watchNamespace)

	srr := &zedgev1alpha1.SecretReplicationRule{
		TypeMeta: metav1.TypeMeta{
			Kind: "SecretReplicationRule",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
			UID:       "example-rule-uid-srr-1",
		},
		Spec: zedgev1alpha1.SecretReplicationRuleSpec{
			WatchNamespace:     watchNamespace,
			NamespaceBlacklist: defaultBlacklist(),
		},
	}

	s2 := createSecret(myCredentialsSecretsName, defaultNamespace)
	s2.OwnerReferences = []metav1.OwnerReference{*metav1.NewControllerRef(srr, controllerKind)}

	// List of resources that will be created inside the mocked client.
	objs := []runtime.Object{
		ns,
		nsDefault,
		nsKubeSystem,
		s2,
		s1,
		srr,
	}

	// Register operator types with the runtime scheme.
	s := scheme.Scheme
	s.AddKnownTypes(zedgev1alpha1.SchemeGroupVersion, srr)
	// Create a fake client to mock API calls.
	cl := fake.NewFakeClient(objs...)

	// Create a ReconcileSecretReplication object with the scheme and fake client.
	r := &ReconcileSecretReplication{client: cl, scheme: s}

	// Mock request to simulate Reconcile() being called on an event for a
	// watched resource .
	req := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      name,
			Namespace: namespace,
		},
	}
	res, err := r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if res.Requeue {
		t.Error("current state in cluster shouldn't have caused reconciles. see ojbs.")
	}

	if err := r.client.Delete(context.Background(), s1); err != nil {
		t.Fatal("Error deleting secret in watchNamespace")
	}

	req = reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      name,
			Namespace: namespace,
		},
	}
	res, err = r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if !res.Requeue {
		t.Error("current state in cluster should cause it to delete secrets")
	}

	secrets, err := getSecretsAllNamespaces(r)
	require.NoError(t, err)
	assert.NotContains(t, secrets, watchNamespace)
	assert.NotContains(t, secrets, defaultNamespace)

	srr = reconcileExampleRule(r, t)
	assert.Len(t, srr.Status.Namespaces, 0)
}

func TestAdoptSecret(t *testing.T) {
	// Set the logger to development mode for verbose logs.
	logf.SetLogger(logf.ZapLogger(true))

	ns := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: watchNamespace,
		},
	}
	nsDefault := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: defaultNamespace,
		},
	}
	nsKubeSystem := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: kubeSystemNamespace,
		},
	}

	// Secret 1
	s1 := createSecret(myCredentialsSecretsName, watchNamespace)

	srr := &zedgev1alpha1.SecretReplicationRule{
		TypeMeta: metav1.TypeMeta{
			Kind: "SecretReplicationRule",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
			UID:       "example-rule-uid-srr-1",
		},
		Spec: zedgev1alpha1.SecretReplicationRuleSpec{
			WatchNamespace:     watchNamespace,
			NamespaceBlacklist: defaultBlacklist(),
			AdoptSecrets:       true,
		},
	}

	s2 := createSecret(myCredentialsSecretsName, defaultNamespace)

	// List of resources that will be created inside the mocked client.
	objs := []runtime.Object{
		ns,
		nsDefault,
		nsKubeSystem,
		s2,
		s1,
		srr,
	}

	// Register operator types with the runtime scheme.
	s := scheme.Scheme
	s.AddKnownTypes(zedgev1alpha1.SchemeGroupVersion, srr)
	// Create a fake client to mock API calls.
	cl := fake.NewFakeClient(objs...)

	// Create a ReconcileSecretReplication object with the scheme and fake client.
	r := &ReconcileSecretReplication{client: cl, scheme: s}

	// Mock request to simulate Reconcile() being called on an event for a
	// watched resource .
	req := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      name,
			Namespace: namespace,
		},
	}
	res, err := r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if !res.Requeue {
		t.Error("current state in cluster should cause it to take ownership of secret in default namespace")
	}

	secrets, err := getSecretsAllNamespaces(r)
	require.NoError(t, err)
	assert.Contains(t, secrets, watchNamespace)
	assert.Contains(t, secrets, defaultNamespace)
	ownerReferences := secrets[defaultNamespace][myCredentialsSecretsName].ObjectMeta.OwnerReferences
	assert.Len(t, ownerReferences, 1)
	assert.Equal(t, srr.UID, ownerReferences[0].UID)
	assert.Equal(t, srr.Name, ownerReferences[0].Name)
	assert.Equal(t, srr.Kind, ownerReferences[0].Kind)
	srr = reconcileExampleRule(r, t)

	assert.Len(t, srr.Status.Namespaces, 1)
}

func TestModifyLabelsOrAnnotationsOrdataReplicatesSourceChanges(t *testing.T) {
	// Set the logger to development mode for verbose logs.
	logf.SetLogger(logf.ZapLogger(true))

	ns := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: watchNamespace,
		},
	}
	nsDefault := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: defaultNamespace,
		},
	}
	nsKubeSystem := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: kubeSystemNamespace,
		},
	}

	// Secret 1
	s1 := createSecret(myCredentialsSecretsName, watchNamespace)

	srr := &zedgev1alpha1.SecretReplicationRule{
		TypeMeta: metav1.TypeMeta{
			Kind: "SecretReplicationRule",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
			UID:       "example-rule-uid-srr-1",
		},
		Spec: zedgev1alpha1.SecretReplicationRuleSpec{
			WatchNamespace:     watchNamespace,
			NamespaceBlacklist: defaultBlacklist(),
		},
	}

	s2 := createSecret(myCredentialsSecretsName, defaultNamespace)
	s2.OwnerReferences = []metav1.OwnerReference{*metav1.NewControllerRef(srr, controllerKind)}

	// List of resources that will be created inside the mocked client.
	objs := []runtime.Object{
		ns,
		nsDefault,
		nsKubeSystem,
		s2,
		s1,
		srr,
	}

	// Register operator types with the runtime scheme.
	s := scheme.Scheme
	s.AddKnownTypes(zedgev1alpha1.SchemeGroupVersion, srr)
	// Create a fake client to mock API calls.
	cl := fake.NewFakeClient(objs...)

	// Create a ReconcileSecretReplication object with the scheme and fake client.
	r := &ReconcileSecretReplication{client: cl, scheme: s}

	// Mock request to simulate Reconcile() being called on an event for a
	// watched resource .
	req := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      name,
			Namespace: namespace,
		},
	}
	res, err := r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if res.Requeue {
		t.Error("current state in cluster shouldn't have caused reconciles. see ojbs.")
	}

	s1.ObjectMeta.Labels = map[string]string{
		"app": "applicationLabel",
	}
	if err := r.client.Update(context.Background(), s1); err != nil {
		t.Fatal("Error updating secret in watchNamespace with labels")
	}

	req = reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      name,
			Namespace: namespace,
		},
	}
	res, err = r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if !res.Requeue {
		t.Error("current state in cluster should cause it re-replicate secrets to update labels on secret")
	}

	secrets, err := getSecretsAllNamespaces(r)
	require.NoError(t, err)
	assert.Contains(t, secrets, watchNamespace)
	assert.Contains(t, secrets, defaultNamespace)

	verifySecret := &corev1.Secret{}

	if err = r.client.Get(
		context.Background(),
		types.NamespacedName{Namespace: defaultNamespace, Name: myCredentialsSecretsName},
		verifySecret); err != nil {
		t.Error("failed to get secret from cluster to verify labels are on replicated secret", err)
	}

	assert.Equal(t, map[string]string{"app": "applicationLabel"}, verifySecret.ObjectMeta.Labels)

	srr = reconcileExampleRule(r, t)
	assert.Len(t, srr.Status.Namespaces, 1)
}

func TestDontReplicateServiceAccountTokens(t *testing.T) {
	// Set the logger to development mode for verbose logs.
	logf.SetLogger(logf.ZapLogger(true))

	ns := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: watchNamespace,
		},
	}
	nsDefault := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: defaultNamespace,
		},
	}
	nsKubeSystem := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: kubeSystemNamespace,
		},
	}

	// Secret 1
	data := make(map[string][]byte)
	data["ca.crt"] = []byte("ca-data")
	data["token"] = []byte("token")
	s1 := &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "secretAccountToken",
			Namespace: watchNamespace,
			UID:       types.UID("secretAccountToken" + "-uid"),
			Annotations: map[string]string{
				"kubernetes.io/service-account.name": "foo",
				"kubernetes.io/service-account.uid":  "foo-uid",
			},
		},
		Data: data,
		Type: "kubernetes.io/service-account-token",
	}

	srr := &zedgev1alpha1.SecretReplicationRule{
		TypeMeta: metav1.TypeMeta{
			Kind: "SecretReplicationRule",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
			UID:       "example-rule-uid-srr-1",
		},
		Spec: zedgev1alpha1.SecretReplicationRuleSpec{
			WatchNamespace:     watchNamespace,
			NamespaceBlacklist: defaultBlacklist(),
		},
	}

	// List of resources that will be created inside the mocked client.
	objs := []runtime.Object{
		ns,
		nsDefault,
		nsKubeSystem,
		s1,
		srr,
	}

	// Register operator types with the runtime scheme.
	s := scheme.Scheme
	s.AddKnownTypes(zedgev1alpha1.SchemeGroupVersion, srr)
	// Create a fake client to mock API calls.
	cl := fake.NewFakeClient(objs...)

	// Create a ReconcileSecretReplication object with the scheme and fake client.
	r := &ReconcileSecretReplication{client: cl, scheme: s}

	// Mock request to simulate Reconcile() being called on an event for a
	// watched resource .
	req := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      name,
			Namespace: namespace,
		},
	}
	res, err := r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if res.Requeue {
		t.Error("current state in cluster shouldn't have caused reconciles. see ojbs.")
	}

	secrets, err := getSecretsAllNamespaces(r)
	require.NoError(t, err)
	assert.Contains(t, secrets, watchNamespace)
	assert.NotContains(t, secrets, defaultNamespace)

	srr = reconcileExampleRule(r, t)
	assert.Len(t, srr.Status.Namespaces, 0)
}

func TestDontReplicateBlacklistedNamespaces(t *testing.T) {
	logf.SetLogger(logf.ZapLogger(true))

	ns := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: watchNamespace,
		},
	}
	nsKubeSystem := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: kubeSystemNamespace,
		},
	}

	// Secret 1
	s1 := createSecret(myCredentialsSecretsName, watchNamespace)

	srr := &zedgev1alpha1.SecretReplicationRule{
		TypeMeta: metav1.TypeMeta{
			Kind: "SecretReplicationRule",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
			UID:       "example-rule-uid-srr-1",
		},
		Spec: zedgev1alpha1.SecretReplicationRuleSpec{
			WatchNamespace:     watchNamespace,
			NamespaceBlacklist: defaultBlacklist(),
		},
	}

	// List of resources that will be created inside the mocked client.
	objs := []runtime.Object{
		ns,
		nsKubeSystem,
		s1,
		srr,
	}

	// Register operator types with the runtime scheme.
	s := scheme.Scheme
	s.AddKnownTypes(zedgev1alpha1.SchemeGroupVersion, srr)
	// Create a fake client to mock API calls.
	cl := fake.NewFakeClient(objs...)

	// Create a ReconcileSecretReplication object with the scheme and fake client.
	r := &ReconcileSecretReplication{client: cl, scheme: s}

	// Mock request to simulate Reconcile() being called on an event for a
	// watched resource .
	req := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      name,
			Namespace: namespace,
		},
	}
	res, err := r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if res.Requeue {
		t.Error("current state in cluster shouldn't have caused reconciles. see ojbs.")
	}

	req = reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      name,
			Namespace: namespace,
		},
	}
	res, err = r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if res.Requeue {
		t.Error("current state shouldnt it cause to reconcile to replicate in to kube-system which is blacklisted")
	}

	secrets, err := getSecretsAllNamespaces(r)
	require.NoError(t, err)
	assert.Contains(t, secrets, watchNamespace)
	for _, blacklistedNamespace := range defaultBlacklist() {
		assert.NotContains(t, secrets, blacklistedNamespace)
	}

	srr = reconcileExampleRule(r, t)
	assert.Len(t, srr.Status.Namespaces, 0)
}

func TestDontReplicateBlacklistedSecrets_NameMatch(t *testing.T) {
	// Set the logger to development mode for verbose logs.
	logf.SetLogger(logf.ZapLogger(true))

	ns := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: watchNamespace,
		},
	}
	nsDefault := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: defaultNamespace,
		},
	}
	nsKubeSystem := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: kubeSystemNamespace,
		},
	}

	// Secret 1
	s1 := createSecret(myCredentialsSecretsName, watchNamespace)

	srr := &zedgev1alpha1.SecretReplicationRule{
		TypeMeta: metav1.TypeMeta{
			Kind: "SecretReplicationRule",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: zedgev1alpha1.SecretReplicationRuleSpec{
			WatchNamespace:     watchNamespace,
			NamespaceBlacklist: defaultBlacklist(),
			SecretBlacklist: []zedgev1alpha1.SecretBlacklistEntry{
				{
					Name: myCredentialsSecretsName,
				},
			},
		},
	}

	// List of resources that will be created inside the mocked client.
	objs := []runtime.Object{
		srr,
		ns,
		nsDefault,
		nsKubeSystem,
		s1,
	}

	// Register operator types with the runtime scheme.
	s := scheme.Scheme
	s.AddKnownTypes(zedgev1alpha1.SchemeGroupVersion, srr)
	// Create a fake client to mock API calls.
	cl := fake.NewFakeClient(objs...)

	// Create a ReconcileSecretReplication object with the scheme and fake client.
	r := &ReconcileSecretReplication{client: cl, scheme: s}

	// Mock request to simulate Reconcile() being called on an event for a
	// watched resource .
	req := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      name,
			Namespace: namespace,
		},
	}
	res, err := r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if res.Requeue {
		t.Error("did not expect reconciles when my-credentials is blacklisted")
	}

	secrets, err := getSecretsAllNamespaces(r)
	require.NoError(t, err)

	assert.Contains(t, secrets, "it")
	assert.NotContains(t, secrets, "kube-system")
	assert.NotContains(t, secrets, "default")

	srr = reconcileExampleRule(r, t)
	assert.Len(t, srr.Status.Namespaces, 0)
}

func TestDontReplicateBlacklistedSecrets_TypeMatch(t *testing.T) {
	// Set the logger to development mode for verbose logs.
	logf.SetLogger(logf.ZapLogger(true))

	ns := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: watchNamespace,
		},
	}
	nsDefault := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: defaultNamespace,
		},
	}
	nsKubeSystem := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: kubeSystemNamespace,
		},
	}

	// Secret 1
	s1 := createSecret(myCredentialsSecretsName, watchNamespace)

	srr := &zedgev1alpha1.SecretReplicationRule{
		TypeMeta: metav1.TypeMeta{
			Kind: "SecretReplicationRule",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: zedgev1alpha1.SecretReplicationRuleSpec{
			WatchNamespace:     watchNamespace,
			NamespaceBlacklist: defaultBlacklist(),
			SecretBlacklist: []zedgev1alpha1.SecretBlacklistEntry{
				{
					Type: "Opaque",
				},
			},
		},
	}

	// List of resources that will be created inside the mocked client.
	objs := []runtime.Object{
		srr,
		ns,
		nsDefault,
		nsKubeSystem,
		s1,
	}

	// Register operator types with the runtime scheme.
	s := scheme.Scheme
	s.AddKnownTypes(zedgev1alpha1.SchemeGroupVersion, srr)
	// Create a fake client to mock API calls.
	cl := fake.NewFakeClient(objs...)

	// Create a ReconcileSecretReplication object with the scheme and fake client.
	r := &ReconcileSecretReplication{client: cl, scheme: s}

	// Mock request to simulate Reconcile() being called on an event for a
	// watched resource .
	req := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      name,
			Namespace: namespace,
		},
	}
	res, err := r.Reconcile(req)
	if err != nil {
		t.Fatalf("reconcile: (%v)", err)
	}
	// Check the result of reconciliation to make sure it has the desired state.
	if res.Requeue {
		t.Error("did not expect reconciles when my-credentials is blacklisted")
	}

	secrets, err := getSecretsAllNamespaces(r)
	require.NoError(t, err)

	assert.Contains(t, secrets, "it")
	assert.NotContains(t, secrets, "kube-system")
	assert.NotContains(t, secrets, "default")

	srr = reconcileExampleRule(r, t)
	assert.Len(t, srr.Status.Namespaces, 0)
}
