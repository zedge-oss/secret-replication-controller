package controller

import (
	"gitlab.com/zedge/secret-replication-controller/pkg/controller/secretreplication"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, secretreplication.Add)
}
