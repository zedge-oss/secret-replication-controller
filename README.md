# Secret replication controller

Objective: Replicate secrets from a given watched namespace to a list of namespaces.

## WARNING WARNING WARNING

This is more or less prototyping in working, thread carefully.

EXPECT THIS REPOSITORY TO BE REBASED/SQUASHED IN THE FUTURE, AFTER THE FIRST
WORKING ITERATION IS EXECUTED.

Current scretch pad for logic: https://hackmd.io/Gp50yocxSa6jSJsWuIHDPw

## How to run operator locally

```bash
OPERATOR_NAME=replication operator-sdk up local --namespace ""
```

## Hack to compile for now and run manager locally

WATCH_NAMESPACE="" go run ./cmd/manager/...
